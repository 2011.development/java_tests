import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NumberSizer {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        scanner.nextLine();

        String errorMessage = "Could not store %s in type %s";
        List<Class> typeList = new ArrayList<>();

        try {
            Byte.parseByte(number);
            typeList.add(Byte.class);
        } catch (NumberFormatException e) {
            System.out.println(String.format(errorMessage, number, Byte.class.getSimpleName()));
        }

        try {
            Short.parseShort(number);
            typeList.add(Short.class);
        } catch (NumberFormatException e) {
            System.out.println(String.format(errorMessage, number, Short.class.getSimpleName()));
        }

        try {
            Integer.parseInt(number);
            typeList.add(Integer.class);
        } catch (NumberFormatException e) {
            System.out.println(String.format(errorMessage, number, Integer.class.getSimpleName()));
        }

        try {
            Long.parseLong(number);
            typeList.add(Long.class);
        } catch (NumberFormatException e) {
            System.out.println(String.format(errorMessage, number, Long.class.getSimpleName()));
        }

        System.out.println(String.format("The following number %s can be stored using the following types:", number));
        typeList.stream().map(Class::getSimpleName).forEach(System.out::println);
    }
}