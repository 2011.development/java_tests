import java.util.Scanner;

class NumberSizerMod {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long num = scanner.nextLong();
        scanner.nextLine();
        System.out.println("long");
        if (num == (int) num) {
            System.out.println("int");
        }
        if (num == (short) num) {
            System.out.println("short");
        }
    }

}