import org.apache.commons.text.RandomStringGenerator;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Main {

    private static int COUNT = 10;
    private static boolean USE_FAST = false;

    public static void main(String[] args) {
        // Run keys
        EmailGenerator emailGenerator = new EmailGenerator();

        /// Empty collection for further UI login form testing
///////////////////////////////////////////////////////////
        Set positiveMails = new HashSet();/////////////////
        Set negativeMails = new HashSet();/////////////////
        Set limitMails = new HashSet();////////////////////
///////////////////////////////////////////////////////////

        System.out.println("Set for Positive cases");
        //name from 5 symbols, surname between 5-12

        for (int i = 5; i < 12; i++) {
            for (int j = 5; j < 12; j++) {
                positiveMails.addAll(emailGenerator.generatePackOfMails(i, j, COUNT, USE_FAST));
            }
        }
        System.out.println("Set for Negative cases");
        //name leaser than 5 surname either leather 5 or greater 12
        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                negativeMails.addAll(emailGenerator.generatePackOfMails(i, j, COUNT, USE_FAST));
            }
            for (int j = 13; j < 17; j++) {
                negativeMails.addAll(emailGenerator.generatePackOfMails(i, j, COUNT, USE_FAST));
            }
        }
        System.out.println("Set for Limit cases");
        //name is near 255 symbols
        for (int i = 250; i < 255; i++) {
            for (int j = 5; j < 12; j++) {
                limitMails.addAll(emailGenerator.generatePackOfMails(i, j, COUNT, USE_FAST));
            }
        }

        System.out.println(Arrays.toString(positiveMails.toArray()));
        System.out.println(Arrays.toString(negativeMails.toArray()));
        System.out.println(Arrays.toString(limitMails.toArray()));

    }
}


class EmailGenerator {

    private static final String DOMAIN = "@motorola.com";
    private RandomStringGenerator generator;

    public EmailGenerator() {
        generator = new RandomStringGenerator.Builder().withinRange('a', 'z').build();
    }


    public String generateEmailAddress(int nameLen, int surnameLen) {
        // Method for correct email generation
        String mailname = generator.generate(nameLen);
        String mailsureame = generator.generate(surnameLen);

        StringBuilder mailBuilder = new StringBuilder();
        mailBuilder.append(mailname);
        mailBuilder.append(".");
        mailBuilder.append(mailsureame);
        mailBuilder.append(DOMAIN);

        return mailBuilder.toString();
    }

    public Set generatePackOfMails(int nameLen, int surnameLen, int numberToGenerate, boolean useFast) {

        return Stream.generate(() -> generateEmailAddress(nameLen, surnameLen))
                .limit(numberToGenerate).collect(Collectors.toSet());
    }
}


